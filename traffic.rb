require 'uri'
require 'net/http'

class Traffic

@debug = true

 def initialize( urlfile )
 	@urlfile = urlfile
 	# User-Agent List
	@ua = {

		'ie10_win_64' => 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
		'ie10_win_32' => 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)',
		'ie9_win_64' => 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; chromeframe/12.0.742.112)',
		'firefox25_win_64' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0',
		'firefox24_ubuntu_64' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0',
		'firefox24_win_64' => 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0',
		'chrome_win8_64' => 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
		'chrome_linux_64' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5'
	}
 end

def connect ( url )

	uri = URI.parse(url)

	@ua.each do |browser,useragent|
		p "-------------------------------------
		p  Conectandome a #{url} usando #{browser}"
		p uri
		p uri.path
		p uri.host
		p uri.port
		p "-------------------------------------"
		if @debug == true
			p "DEBUG MODE ACTIVE ---"
			useragent = useragent + ' DEBUG'
		end

		request = Net::HTTP::Get.new(uri.path)
		request['User-Agent'] = useragent
		request['Content-Type'] = 'text/html'
		request['Accept'] = 'text/html'
		request['Referer'] = 'http://www.eidast.com'

		http = Net::HTTP.new(uri.host, uri.port)

		response = http.request(Net::HTTP::Get.new(uri.request_uri, { 'User-Agent' => useragent }))

		sleep Random.rand(5)

		case response

		when Net::HTTPSuccess then response
		when Net::HTTPRedirection then connect(response['location'])

		end

	end

end

def readFile 
 	File.open(@urlfile, "r").each_line do |url|
 		connect url.chomp
 	end
 end

end

def help
 	print "Faltan parametros
Uso: #{__FILE__} urlsfile"
end

if ARGV.size == 0 
	help
	exit
end

ifile = ARGV.join ' '

t = Traffic.new ifile

t.readFile